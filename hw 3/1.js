/*1) функции нужны для повторного использования кода. с помощью них также можго обратиться к данному фрагменту кода из
 любого другого места программы.
* 2) аргументы передаются для того, чтобы можно было одни и те же действия с разными данными. */


let num1 = prompt('Enter first number, please');
let num2 = prompt('Enter second number, please');
let method = prompt('Enter math operator, please');

let count = function () {
    switch (method) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "*":
            return num1 * num2;
        case "/":
            if (num2 !== 0) {
                return num1 / num2;
            } else {
            return "На ноль делить нельзя!";
        }
    }

    console.log(count(num1, num2, method));

};