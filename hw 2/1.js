/* циклы нужны для ситуаций, когда один и тот же код необходимо повторять множество раз при каком-то оприделённом
 условии. они позволяют существенно сократить объемы кода при написании и сэкономить память. */

let a = prompt('Enter your number, please');
let start = 0;
let answer = '';

if (a && a >= 0) {
    for (let i = 0; i <= a; i++) {
        if (a % 5 === 0) {
            answer = `${i}`;
        }
    }
}

if (!start) {
    console.log('Sorry, no numbers!');
} else {
    console.log(answer);
}
