/* 1) var переменные ограничены функцией, а let и const - блоком. разница между var, let и const заключается в том, что
 в первых двух можно изменить значение переменной, в последней же нельзя.
  2) объявлять переменную через var считается плохим тоном, потому что её значение можно легко переназначить и это
   заденет весь код */

let name = prompt('Enter your name, please.');

while ( name === '' || name === null ) {
    name = prompt('Something went wrong. Please, try again.')
}

let age = prompt('Enter your age, please.')

while ( age === null || Number.isNaN(+age)) {
    age = prompt('Something went wrong. Please, try again.')
}

if ( age < 18 ) {
    alert(`You are not allowed to visit this website, ${name}` );
} else if ( age >= 18 && age <= 22 ) {
    let confirmation = confirm('Are you sure you want to continue?');
    if (confirmation) {
        alert(`Welcome, ${name}`);
    } else if ( age > 22) {
        alert(`Welcome, ${name}`)
    }
}